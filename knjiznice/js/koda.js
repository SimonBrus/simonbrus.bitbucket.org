/*global $*/
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var chart = null;

var ehrMiha="678e0fbb-0e7c-4968-93ad-c96cceeaac0c";
var ehrMaja="2da6a5b5-e33a-484c-a149-1dfc3346d278";
var ehrBob="49c03023-1ce1-4068-b8fa-057fc435a628";

/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke() {
  generirajMiha();
}
function generirajMiha(){
  dodajPacienta("Miha", "Leteči", "1988-09-09","m");
  setTimeout(function(){
    var ehrId = $("#EhrId").html();
    ehrMiha = ehrId;
    dodajSpecificneMeritve(ehrId, 178, 125, 36.5, 72, 85, 50);
    dodajSpecificneMeritve(ehrId, 178, 120, 36, 74, 80, 50);
    dodajSpecificneMeritve(ehrId, 178, 125, 36.8, 76, 84, 50);
    dodajSpecificneMeritve(ehrId, 178, 130, 37, 70, 88, 50);
    generirajMaja();
  }, 200);
}

function generirajMaja(){
  dodajPacienta("Maja", "Lahka", "1997-11-05","f");
  setTimeout(function(){
    var ehrId = $("#EhrId").html();
    ehrMaja = ehrId;
    dodajSpecificneMeritve(ehrId, 158, 125, 36.5, 42, 85, 50);
    dodajSpecificneMeritve(ehrId, 160, 120, 36, 45, 80, 50);
    dodajSpecificneMeritve(ehrId, 162, 125, 36.8, 46, 84, 50);
    dodajSpecificneMeritve(ehrId, 163, 130, 37, 44.5, 88, 50);
    dodajSpecificneMeritve(ehrId, 163, 132, 36.3, 46, 84, 50);
    generirajBob();
  }, 200);
}

function generirajBob(){
  dodajPacienta("Bob", "Silni", "1969-02-03","m");
  setTimeout(function(){
    var ehrId = $("#EhrId").html();
    ehrBob = ehrId;
    dodajSpecificneMeritve(ehrId, 168, 145, 37.5, 130, 100, 50);
    dodajSpecificneMeritve(ehrId, 168, 152, 37, 127, 99, 50);
    dodajSpecificneMeritve(ehrId, 168, 134, 36.8, 117, 89, 50);
    dodajSpecificneMeritve(ehrId, 168, 130, 37, 110, 88, 50);
    dodajSpecificneMeritve(ehrId, 168, 139, 36.3, 109, 92, 50);
  }, 200);
}

function dodajSpecificneMeritve(ehrId, visina, tlakS, temperatura, teza, tlakD, kisik){
  $.ajaxSetup({
    headers: {
        "Authorization": getAuthorization()
    }
  });
  var compositionData = {
    "ctx/time": "2019-05-19",
    "ctx/language": "en",
    "ctx/territory": "CA",
    "vital_signs/body_temperature/any_event/temperature|magnitude": temperatura,
    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
    "vital_signs/blood_pressure/any_event/systolic": tlakS,
    "vital_signs/blood_pressure/any_event/diastolic": tlakD,
    "vital_signs/height_length/any_event/body_height_length": visina,
    "vital_signs/body_weight/any_event/body_weight": teza
  };
  var queryParams = {
    "ehrId": ehrId,
    templateId: 'Vital Signs',
    format: 'FLAT',
    committer: 'Belinda Nurse'
  };
  $.ajax({
    url: baseUrl + "/composition?" + $.param(queryParams),
    type: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(compositionData),
    success: function (res) {
      
    }
  });
}

function selectSvetovanje(){
  var vnos = $("#selectSvetovanje").val();
  if(vnos == 1){
    document.getElementById("vnosEhrIdSvetuj").value = ehrMiha;
  }else if(vnos == 2){
    document.getElementById("vnosEhrIdSvetuj").value = ehrMaja;
  }else if(vnos == 3){
    document.getElementById("vnosEhrIdSvetuj").value = ehrBob;
  }else{
    document.getElementById("vnosEhrIdSvetuj").value = "";
  }
}

function selectVnos(){
  var vnos = $("#selectVnos").val();
  if(vnos == 1){
    document.getElementById("vnosEhrId").value = ehrMiha;
  }else if(vnos == 2){
    document.getElementById("vnosEhrId").value = ehrMaja;
  }else if(vnos == 3){
    document.getElementById("vnosEhrId").value = ehrBob;
  }else{
    document.getElementById("vnosEhrId").value = "";
  }
}

function gumbDodajPacienta(){
    var ime = $("#vnosIme")[0].value;
    var priimek = $("#vnosPriimek")[0].value;
    var rojstvo = $("#vnosDatumRojstva")[0].value;
    if(ime == "" || priimek == "" || rojstvo == ""){
      document.getElementById("uspesnoKreiran").style.color = "red";
      $("#uspesnoKreiran").html(" Neuspešno! Izpolnite vsa vnosna polja.");
      setTimeout(function(){
      $("#uspesnoKreiran").html("");
      }, 3000);
      return;
    }
    var spol = $('input[name=spol]:checked', '#izbiraSpola').val();
    console.log(spol);
    var ehrId = dodajPacienta(ime, priimek, rojstvo,spol);
    document.getElementById("uspesnoKreiran").style.color = "green";
    $("#uspesnoKreiran").html(" Uspešno kreiran uporabnik.");
      setTimeout(function(){
      $("#uspesnoKreiran").html("");
      }, 3000);
}

function dodajPacienta(ime, priimek, rojstvo, spol){
    
    $.ajaxSetup({
      headers: {
          "Authorization": getAuthorization()
      }
    });
  var ehrId = 0;
  $.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      success: function (data) {
          var ehrId = data.ehrId;
          $("#EhrId").html(ehrId);
          // build party data
      var partyData = {
      		            firstNames: ime,
      		            lastNames: priimek,
      		            dateOfBirth: rojstvo,
      		            partyAdditionalInfo: [{key: "ehrId", value: ehrId},{key: "spol", value: spol}]
      		            };
  
          $.ajax({
              url: baseUrl + "/demographics/party",
              type: 'POST',
              contentType: 'application/json',
              data: JSON.stringify(partyData),
              success: function (party) {
                  if (party.action == 'CREATE') {
                      // $("#result").html("Created: " + party.meta.href);
                  }
              },
              error: function(err) {
              //     $("#result").html("<span class='obvestilo label " +
              // "label-danger fade-in'>Napaka '" +
              // JSON.parse(err.responseText).userMessage + "'!");
              }
          });
      }
  });
  return $("#EhrId").html;
}

function dodajMeritve(){
    
  var visina = $("#vnosVisina")[0].value;
  var tlakS = $("#vnosTlakS")[0].value;
  var temperatura = $("#vnosTemperatura")[0].value;
  var teza = $("#vnosTeza")[0].value;
  var tlakD = $("#vnosTlakD")[0].value;
  var kisik = $("#vnosKisik")[0].value;
  var ehrId = $("#vnosEhrId")[0].value;
  
  if(ehrId.length < 25 || visina == "" || tlakS == "" || tlakD == "" || temperatura == "" || teza == "" || kisik == ""){
    document.getElementById("uspesnaMeritev").style.color = "red";
    $("#uspesnaMeritev").html(" Neuspešno! Izpolnite vsa vnosna polja in podajte pravilen EhrID.");
    setTimeout(function(){
      $("#uspesnaMeritev").html("");
    }, 3000);
    return;
  }
  
  
  
  console.log("dodajmeritev ehrid:",ehrId);
  $.ajaxSetup({
    headers: {
        "Authorization": getAuthorization()
    }
  });
  var compositionData = {
    "ctx/time": "2019-05-19",
    "ctx/language": "en",
    "ctx/territory": "CA",
    "vital_signs/body_temperature/any_event/temperature|magnitude": temperatura,
    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
    "vital_signs/blood_pressure/any_event/systolic": tlakS,
    "vital_signs/blood_pressure/any_event/diastolic": tlakD,
    "vital_signs/height_length/any_event/body_height_length": visina,
    "vital_signs/body_weight/any_event/body_weight": teza
  };
  var queryParams = {
    "ehrId": ehrId,
    templateId: 'Vital Signs',
    format: 'FLAT',
    committer: 'Belinda Nurse'
  };
  $.ajax({
    url: baseUrl + "/composition?" + $.param(queryParams),
    type: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(compositionData),
    success: function (res) {
    }
  });
  document.getElementById("uspesnaMeritev").style.color = "green";
  $("#uspesnaMeritev").html(" Uspešno dodana meritev.");
  setTimeout(function(){
    $("#uspesnaMeritev").html("");
  }, 3000);
}

function svetuj(){
  var ehrId = $("#vnosEhrIdSvetuj")[0].value;
  if(ehrId.length < 25){
    document.getElementById("napakaSvetuj").style.color = "red";
    $("#napakaSvetuj").html(" podajte pravilen EhrID.");
    setTimeout(function(){
      $("#napakaSvetuj").html("");
    }, 3000);
    return;
  }
    pridobiPodatke(ehrId);
  }

function pridobiPodatke(ehrId){
  
  
  $.ajaxSetup({
    headers: {
        "Authorization": getAuthorization()
    }
  });
  $.ajax({
    url: baseUrl + "/view/" + ehrId + "/blood_pressure",
    type: 'GET',
    success: function (res) {
        //$("#header").html("Blood pressures");
        $("#tlakS").html(res[res.length - 1].systolic + res[res.length - 1].unit);
        $("#gumbTlakS").removeClass("skrij");
        $("#tlakD").html(res[res.length - 1].diastolic + res[res.length - 1].unit);
        $("#gumbTlakD").removeClass("skrij");
    }
  });
  $.ajax({
    url: baseUrl + "/view/" + ehrId + "/height",
    type: 'GET',
    success: function (res) {
        $("#visina").html(res[res.length - 1].height + res[res.length - 1].unit);
        $("#gumbVisina").removeClass("skrij");
    }
  });
  $.ajax({
    url: baseUrl + "/view/" + ehrId + "/weight",
    type: 'GET',
    success: function (res) {
      console.log("teza:");
      console.log(res);
        $("#teza").html(res[res.length - 1].weight + res[res.length - 1].unit);
        $("#gumbTeza").removeClass("skrij");
    }
  });
  $.ajax({
    url: baseUrl + "/view/" + ehrId + "/body_temperature",
    type: 'GET',
    success: function (res) {
        $("#temperatura").html(res[res.length - 1].temperature + res[res.length - 1].unit);
        $("#gumbTemperatura").removeClass("skrij");
    }
  });
  $.ajax({
    url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
    type: 'GET',

    success: function(data) {
      console.log(data);
      $("#imePriimek").html(data.party.firstNames + " " + data.party.lastNames);
      var starost = 2019 - data.party.dateOfBirth.substring(0,4);
      var spol = data.party.additionalInfo.spol;
      var visina = $("#visina").html();
      visina = visina.substring(0,visina.length-2);
      var teza = $("#teza").html();
      teza = teza.substring(0,teza.length-2);
      bmi(starost, spol, teza, visina);
    }
  });
   
}
function bmi(starost, spol, teza, visina){
  var data = {"weight":{"value": teza,"unit":"kg"},"height":{"value": visina,"unit":"cm"},"sex": spol,"age":starost,"waist":"34.00","hip":"40.00"};
  console.log(data);
  $.ajax({
    headers: {
      "X-RapidAPI-Host": "bmi.p.rapidapi.com",
      "X-RapidAPI-Key": "61dfe55cfamshe0b52bdff7c3862p192b87jsn6d12e581d7af",
      "Content-Type": "application/json"
    },
    url: "https://bmi.p.rapidapi.com/",
    type: 'POST',
    //contentType: 'application/json',
    data: JSON.stringify(data),
    success: function (res) {
      console.log(res);
      $("#bmi").html(res.bmi.value);
      $("#stanje").html(res.bmi.status);
    }
  });
}
function narisiGraf(meritve, atribut, labels, backgroundColors, foregroundColors){
  var ctx = document.getElementById('myChart').getContext('2d');
  if(chart != null){
      chart.clear();
      chart.destroy();
  }
  chart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: labels,
        datasets: [{
            label: atribut,
            data: meritve,
            backgroundColor: backgroundColors,
            borderColor: foregroundColors,
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
  });
}

var resetCanvas = function(){
  var chart = $('#myChart');
  console.log(chart);
   chart.remove(); // this is my <canvas> element
   var container = $('#canvasContainer');
   console.log(container);
  container.append('<canvas id="myChart"></canvas>');
  var canvas = document.getElementById('myChart');
  ctx = canvas.getContext('2d');
  ctx.canvas.width = 150; // resize to parent width
  ctx.canvas.height = 120; // resize to parent height
  var x = canvas.width/2;
  var y = canvas.height/2;
  ctx.font = '10pt Verdana';
  ctx.textAlign = 'center';
  ctx.fillText('This text is centered on the canvas', x, y);
};


function header(){
  $.ajaxSetup({
    headers: {
        "Authorization": getAuthorization()
    }
  });
}
function visinaInfo(){
  header();
  var ehrId = $("#vnosEhrIdSvetuj")[0].value;
  $.ajax({
    url: baseUrl + "/view/" + ehrId + "/height",
    type: 'GET',
    success: function (res) {
      console.log(res);
      var meritve = [];
      var labels = [];
      var backgroundColor = [];
      var foregroundColor = [];
      if(res.length <= 5){
        for (var i = 0; i < res.length; i++) {
          meritve.push(res[i].height);
          var index = Number(i) + 1;
          labels.push(index + ".");
          backgroundColor.push('rgba(255, 99, 132, 0.2)');
          foregroundColor.push('rgba(255, 99, 132, 1)');
        }
      }
      else{
        for(var i = res.length - 5; i < res.length; i++){
          meritve.push(res[i].height);
          var index = Number(i) + 1;
          labels.push(index + ".");
          backgroundColor.push('rgba(255, 99, 132, 0.2)');
          foregroundColor.push('rgba(255, 99, 132, 1)');
        }
      }
      narisiGraf(meritve, "visina(cm)", labels, backgroundColor, foregroundColor);
        
    }
  });
}

function tezaInfo(){
  header();
  var ehrId = $("#vnosEhrIdSvetuj")[0].value;
  $.ajax({
    url: baseUrl + "/view/" + ehrId + "/weight",
    type: 'GET',
    success: function (res) {
      var meritve = [];
      var labels = [];
      var backgroundColor = [];
      var foregroundColor = [];
      if(res.length <= 5){
        for (var i = 0; i < res.length; i++) {
          meritve.push(res[i].weight);
          var index = Number(i) + 1;
          labels.push(index + ".");
          backgroundColor.push('rgba(54, 162, 235, 0.2)');
          foregroundColor.push('rgba(54, 162, 235, 1)');
        }
      }
      else{
        for(var i = res.length - 5; i < res.length; i++){
          meritve.push(res[i].weight);
          var index = Number(i) + 1;
          labels.push(index + ".");
          backgroundColor.push('rgba(54, 162, 235, 0.2)');
          foregroundColor.push('rgba(54, 162, 235, 1)');
        }
      }
      narisiGraf(meritve, "teža(kg)", labels, backgroundColor, foregroundColor);
        
    }
  });
}

function temperaturaInfo(){
  header();
  var ehrId = $("#vnosEhrIdSvetuj")[0].value;
  $.ajax({
    url: baseUrl + "/view/" + ehrId + "/body_temperature",
    type: 'GET',
    success: function (res) {
      var meritve = [];
      var labels = [];
      var backgroundColor = [];
      var foregroundColor = [];
      if(res.length <= 5){
        for (var i = 0; i < res.length; i++) {
          meritve.push(res[i].temperature);
          var index = Number(i) + 1;
          labels.push(index + ".");
          backgroundColor.push('rgba(255, 206, 86, 0.2)');
          foregroundColor.push('rgba(255, 206, 86, 1)');
        }
      }
      else{
        for(var i = res.length - 5; i < res.length; i++){
          meritve.push(res[i].temperature);
          var index = Number(i) + 1;
          labels.push(index + ".");
          backgroundColor.push('rgba(255, 206, 86, 0.2)');
          foregroundColor.push('rgba(255, 206, 86, 1)');
        }
      }
      narisiGraf(meritve, "temperatura(°C)", labels, backgroundColor, foregroundColor);
        
    }
  });
}

function tlakSInfo(){
  header();
  var ehrId = $("#vnosEhrIdSvetuj")[0].value;
  $.ajax({
    url: baseUrl + "/view/" + ehrId + "/blood_pressure",
    type: 'GET',
    success: function (res) {
      var meritve = [];
      var labels = [];
      var backgroundColor = [];
      var foregroundColor = [];
      if(res.length <= 5){
        for (var i = 0; i < res.length; i++) {
          meritve.push(res[i].systolic);
          var index = Number(i) + 1;
          labels.push(index + ".");
          backgroundColor.push('rgba(75, 192, 192, 0.2)');
          foregroundColor.push('rgba(75, 192, 192, 1)');
        }
      }
      else{
        for(var i = res.length - 5; i < res.length; i++){
          meritve.push(res[i].systolic);
          var index = Number(i) + 1;
          labels.push(index + ".");
          backgroundColor.push('rgba(75, 192, 192, 0.2)');
          foregroundColor.push('rgba(75, 192, 192, 1)');
        }
      }
      narisiGraf(meritve, "Sistolični tlak(mm[Hg])", labels, backgroundColor, foregroundColor);
        
    }
  });
}

function tlakDInfo(){
  header();
  var ehrId = $("#vnosEhrIdSvetuj")[0].value;
  $.ajax({
    url: baseUrl + "/view/" + ehrId + "/blood_pressure",
    type: 'GET',
    success: function (res) {
      var meritve = [];
      var labels = [];
      var backgroundColor = [];
      var foregroundColor = [];
      if(res.length <= 5){
        for (var i = 0; i < res.length; i++) {
          meritve.push(res[i].diastolic);
          var index = Number(i) + 1;
          labels.push(index + ".");
          backgroundColor.push('rgba(153, 102, 255, 0.2)');
          foregroundColor.push('rgba(153, 102, 255, 1)');
        }
      }
      else{
        for(var i = res.length - 5; i < res.length; i++){
          meritve.push(res[i].diastolic);
          var index = Number(i) + 1;
          labels.push(index + ".");
          backgroundColor.push('rgba(153, 102, 255, 0.2)');
          foregroundColor.push('rgba(153, 102, 255, 1)');
        }
      }
      narisiGraf(meritve, "Diastolični tlak(mm[Hg])", labels, backgroundColor, foregroundColor);
        
    }
  });
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
