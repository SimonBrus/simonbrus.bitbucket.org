/* global L, distance */
//spremenljivke za delo z mapo
var pot;
var markerji = [];
var mapa;
var obmocje;
const FRI_LAT = 46.05004;
const FRI_LNG = 14.46931;
var linkBolnisnice = "https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json";

window.addEventListener('load', function () {


	// Osnovne lastnosti mape
	var mapOptions = {
		center: [FRI_LAT, FRI_LNG],
		zoom: 12
		// maxZoom: 3
	};
		
	// Ustvarimo objekt mapa
	mapa = new L.map('mapa_id', mapOptions);
	
	// Ustvarimo prikazni sloj mape
	var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
		
	// Prikazni sloj dodamo na mapo
	mapa.addLayer(layer);
		  
	// Objekt oblačka markerja
	var popup = L.popup();
	
	function obKlikuNaMapo(e) {
		var latlng = e.latlng;
		    
		popup
			.setLatLng(latlng)
			.setContent("Izbrana točka:" + latlng.toString())
			.openOn(mapa);
	}

    mapa.on('click', obKlikuNaMapo);
    
    // create a red polygon from an array of LatLng points
	var latlngs = [[37, -109.05],[41, -109.03],[41, -102.05],[37, -102.04]];
	var polygon = L.polygon(latlngs, {color: 'red'}).addTo(mapa);
	// zoom the map to the polygon
	//mapa.fitBounds(polygon.getBounds());
	
	
	$.ajax({
    url: linkBolnisnice,
    type: 'GET',
    success: function (res) {
        console.log(res.features[97]);
        for(var feature in res.features){
        	narisiBolnico(res.features[feature].geometry.coordinates);
        	//console.log(feature);
        }
        
    }
  });
  
});

function narisiBolnico(coords){
	var i;
	for (i = 0; i < coords[0].length; i++) { 
		var zac = coords[0][i][0];
		coords[0][i][0] = coords[0][i][1];
		coords[0][i][1] = zac;
	}
	if(coords.length > 1){
		for (i = 0; i < coords[1].length; i++) { 
			var zac = coords[1][i][0];
			coords[1][i][0] = coords[1][i][1];
			coords[1][i][1] = zac;
		}
	}
	console.log(coords[0][0]);
	var latlngs =coords;
	var polygon = L.polygon(latlngs, {color: 'blue'}).addTo(mapa);
	//mapa.fitBounds(polygon.getBounds());
}